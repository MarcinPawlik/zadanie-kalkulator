<?php

class CurrencyTableSeeder extends Seeder
{

    public function run()
    {
        $currency = ['EUR/PLN' => 3.89, 'USD/PLN' => 3.54, 'CHF/PLN' => 2.99];

        foreach ($currency as $name => $val) {
            $c        = new Currency();
            $c->name  = $name;
            $c->value = $val;
            $c->save();
        }
    }
}