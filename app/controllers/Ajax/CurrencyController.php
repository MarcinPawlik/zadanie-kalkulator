<?php

class Ajax_CurrencyController extends BaseController
{
    //rules for validation
    private $rules = [
        'money' => 'required|numeric|digits_between:1,1000000',
    ];

    public function get($currency)
    {
        return Response::json(['type' => 'success', 'name' => $currency->name, 'value' => $currency->value]);
    }

    public function calc($currency)
    {
        $data = Input::get();

        $validator = Validator::make($data, $this->rules);
        if ($validator->fails()) {
            $messages = $validator->messages();
            foreach ($messages->all() as $msg) {
                return Response::json([
                        'type' => 'error',
                        'message' => $msg,
                ]);
            }
        }
        $name = explode('/', $currency->name);
        $from = $name[0];
        $to   = $name[1];
        return Response::json([
                'type' => 'success',
                'form' => $from,
                'to' => $to,
                'money' => ($data['money'] * $currency->value)
        ]);
    }
}