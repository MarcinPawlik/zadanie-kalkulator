<?php

class CurrencyController extends BaseController
{

    //Lists currency
    public function index()
    {
        $currency = Currency::lists('name', 'id');
        return View::make('currency.index')
                ->withCurrency($currency);
    }
}