<?php

class Currency extends Eloquent
{
    protected $table    = 'currency';
    protected $fillable = ['name', 'value']; //white list
    protected $guarded  = []; //black list

}