<?php
Route::model('currency', 'Currency');

Route::get('/', 'CurrencyController@index');
Route::get('/ajax/get/{currency}', 'Ajax_CurrencyController@get');
Route::post('/ajax/calc/{currency}', 'Ajax_CurrencyController@calc');

App::missing(function($e) {
    return 'error 404';
});
