<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        {{ HTML::style('css/bootstrap.min.css') }}
        {{ HTML::script('js/jquery.min.js') }}

    </head>
    <body>

        <div class="container">
            @yield('content')
        </div>
        <script>
            var baseURL = "{{ URL::to('/') }}";
        </script>
        {{ HTML::script('js/currency.js') }}
    </body>
</html>
