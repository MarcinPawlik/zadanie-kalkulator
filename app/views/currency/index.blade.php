@extends('layout.master')

@section('content')
<h1>Kalkulator Walut</h1>

<div class="box">

    <table class="table">
        <tbody>
            <tr>
                <td colspan="2"><label for="rates">Kurs</label></td>
                <td>
                    {{ Form::select('currency', $currency, null, ['id' => 'currency', 'class' => 'form-control']) }}
                </td>
            </tr>
            <tr>
                <td>Aktualny kurs</td>
                <td id="rate_name"></td>
                <td id="rate_value"></td>
            </tr>
            <tr>
                <td colspan="2">Kwota do przeliczenia:</td>
                <td>
                    <input id="money" type="text" name="money" value="" class="form-control" placeholder="kwota..." />
                </td>
            </tr>
            <tr>
                <td colspan="3">
                    <button id="calc" type="submit"  class="btn btn-default">Przelicz</button>
                </td>
            </tr>
        </tbody>

        </form>

        <div id="result" style="display: none;" class="alert alert-success">
            <span id="amount"></span><span id="currency_from"></span> warte jest <span id="value"></span><span id="currency_to"></span>
        </div>
        <div class="errors alert alert-danger" id="errors" style="display: none;"></div>

</div>
@stop