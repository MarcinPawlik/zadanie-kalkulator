
$(document).ready(function () {
    getCurrency();

    $("#currency").change(function () {
        getCurrency();
        if ($('#result').css('display') !== 'none') {
            calc();
        }
    });

    $("#calc").click(function () {
        calc();
    });
});

function getId() {
    $id = 0;
    $("#currency option:selected").each(function () {
        $id = $(this).val();
    });
    return $id;
}

function getCurrency() {
    $id = getId();
    $.get(baseURL + "/ajax/get/" + $id, function (data) {
        if (data.type === 'success') {
            $("#rate_name").text(data.name);
            $("#rate_value").text(data.value);
        } else {
            //to do.
        }
    });
}

function calc() {
    $id = getId();

    $("#errors").hide();
    $("#errors").text('');

    $money = $("#money").val();
    $.post(baseURL + "/ajax/calc/" + $id, {money: $money}).done(function (data) {
        if (data.type === "success") {
            $("#currency_from").text(data.form);
            $("#currency_to").text(data.to);
            $("#value").text(data.money);
            $("#result").show();
        } else {
            $("#errors").append(data.message);
            $("#errors").show();
        }
    });
}
